#!/bin/sh

cd /var/www

if [ "$NODE_ENV" = "production" ]; then
  echo "BUILD PRODUCTION"
  npm run build
else 
  echo "BUILD DEVELOPMENT"
  npm run ft-install
fi 

npm start
