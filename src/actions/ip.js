import * as c from 'constants/actions'

export function getIPRequest() {
  return {
    type: c.API_GET_CURRENT_IP_REQUEST
  }
}
export function getIPSuccess(data) {
  return {
    type: c.API_GET_CURRENT_IP_SUCCESS,
    payload: data
  }
}
export function getIPError() {
  return {
    type: c.API_GET_CURRENT_IP_ERROR
  }
}
