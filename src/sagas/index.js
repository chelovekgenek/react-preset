import { fork, all } from 'redux-saga/effects'

import watcherHandleIPRequest from './ip'

export default function* rootSaga() {
  yield all([
    fork(watcherHandleIPRequest)
  ])
}
