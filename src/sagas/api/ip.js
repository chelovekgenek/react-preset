import axios from 'axios'

import { URL_CURRENT_IP } from 'constants/index'

export function getIP() {
  return (
    axios
      .get(URL_CURRENT_IP)
      .then(result => result.data)
  )
}
