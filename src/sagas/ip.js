import { call, put, takeEvery } from 'redux-saga/effects'

import { getIPSuccess, getIPError } from 'actions/ip'
import * as c from 'constants/actions'
import { getIP } from './api/ip'

function* handleIPRequest() {
  try {
    const result = yield call(getIP)

    yield put(getIPSuccess(result.ip))
  } catch (error) {
    yield put(getIPError())
  }
}

// TODO: remove default, it's only for linter now
export default function* watcherHandleIPRequest() {
  yield takeEvery(c.API_GET_CURRENT_IP_REQUEST, handleIPRequest)
}
