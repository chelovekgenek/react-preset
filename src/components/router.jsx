import { Router, Route, Redirect, IndexRedirect } from 'react-router'

import { App } from 'components/containers'
import { Example } from 'components/routes'

import * as r from 'constants/routes'

const renderRoutes = ({ history }) => (
  <Router history={history}>
    <Route path={r.INDEX} component={App} >
      <IndexRedirect to={r.EXAMPLE} />
      <Route path={r.EXAMPLE} component={Example} />
      <Redirect from={r.ANY} to={r.INDEX} />
    </Route>
  </Router>
)

renderRoutes.propTypes = {
  history: PropTypes.object.isRequired
}

export default renderRoutes
