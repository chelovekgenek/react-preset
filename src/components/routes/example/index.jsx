import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getIPRequest } from 'actions/ip'

import ExampleTest from './example.stateless'

import styles from './example.module.scss'

class Example extends React.PureComponent {
  static defaultProps = {
    app: {
      ip: '0.0.0.0',
      error: false
    }
  }

  componentDidMount() {
    this.props.getIPRequest()
  }

  render() {
    return (
      <div className='aloha'>
        { this.props.app.error
          ? <h1 className={styles.error}>error while fetching</h1>
          : <ExampleTest ip={this.props.app.ip} />
        }
      </div>
    )
  }
}

Example.propTypes = {
  getIPRequest: PropTypes.func.isRequired,
  app: PropTypes.shape({
    ip: PropTypes.string,
    error: PropTypes.bool
  })
}

function mapStateToProps({ rootReducer }) {
  return {
    app: rootReducer.app
  }
}
function mapDispatchToProps(dispatch) {
  return {
    getIPRequest: bindActionCreators(getIPRequest, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Example)
