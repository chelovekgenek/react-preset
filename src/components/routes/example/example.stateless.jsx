const example = ({ ip = '' }) => (<div>Your ip is: { ip || 'fetching....' }</div>)

example.defaultProps = {
  ip: ''
}

example.propTypes = {
  ip: PropTypes.string
}

export default example
