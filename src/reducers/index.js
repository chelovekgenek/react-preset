import { combineReducers } from 'redux'

import app from './app'

const rootReducer = combineReducers({
  app
  // ...your other reducers here
})

export default rootReducer
