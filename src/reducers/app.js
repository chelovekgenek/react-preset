import * as c from 'constants/actions'

const initialState = {
  ip: '',
  fetching: false,
  error: false
}

export default function (state = initialState, action) {
  switch (action.type) {
    case c.API_GET_CURRENT_IP_REQUEST:
      return { ...state, fetching: true }
    case c.API_GET_CURRENT_IP_SUCCESS:
      return { ...state, fetching: false, ip: action.payload }
    case c.API_GET_CURRENT_IP_ERROR:
      return { ...state, fetching: false, error: true }
    default:
      return state
  }
}
