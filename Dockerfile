FROM node:8.4.0

ENV APP_DIR=/var/www
ENV APP_PORT=80
ENV NODE_ENV=production

COPY ./docker/run.sh /bin/run.sh
COPY . $APP_DIR 

WORKDIR $APP_DIR

RUN ls \
 && npm install \
 && npm rebuild node-sass --force \
 && chmod +x /bin/*

EXPOSE $APP_PORT

CMD ["run.sh"]